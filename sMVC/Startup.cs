﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(sMVC.Startup))]
namespace sMVC
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
